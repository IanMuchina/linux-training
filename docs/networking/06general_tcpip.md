tcp and udp
==========

Besides `tcp`, we also find` udp` in `layer 4`. You noted that
before sending an `http request`, first one
`tcp handshake` takes place, but that for the` dns query` completely
was not a handshake. That's because `http` uses` tcp` in it
`layer 4`, while` dns` works on top of `udp`.

tcp
---

`tcp`tcp stands for` Transmission Control Protocol`. It is a protocol
that establishes a `connection` before sending data. As your security
for your packets to arrive, `tcp` is the preferred protocol. In
the `tcp header` has a lot of overhead. As a `tcp pack` not
it will be sent again.

The current `tcp` standard was enshrined in` rfc 793` from 1981.

The `tcp triple handshake`tcp handshake always precedes the` tcp`
connection. This means that there are at least four packets about it
network.

! [] (images / tcp_sessie.png)

udp
---

Unlike `tcp`, there is no connection at` udp`udp, and also
no handshake. It is called `udp` a` connectionless` protocol.

`udp` has less overhead than` tcp` and is therefore faster for it
forwarding of data. But `udp` does not check or packet either
does arrive. As an `udp` pack, its destination is not
you have lost it.

! [] (images / udp.png)

practice
--------

Think for yourself when you would use udp, and when tcp.

email from your manager?

website of a customer?

live radio broadcast of Clijsters against Henin?

dns?

ping and arp
===========

The `arp` protocol is a` layer 2` protocol that links
the `ip address` and` mac address` of a computer. We have in the classroom
this effect is demonstrated using the `ping` command and the
`wireshark` sniffer. We also ran the `arp` command every
View `arp-cache-table`.

packets
=========

I have regularly drawn 'packets' on the board. Technical speaking
we are of an `ethernet frame` and of an` ip datagram`. The exact format
(per bit or byte) of these packets does not belong to the subject matter. I
here just assume that you know an ethernet network card
the first six bytes, being the destination mac address compares to
her own mac address.

Below an `ethernet frame` with the full size bytes and some
info in the right position.

! [] (images / ethernet_frame.png)

Because we need to know something about `mac addresses`,` ip addresses` and
We use `ports` to understand protocols and devices
The simplified representation of a `packet` on the
network.

! [] (images / simple_packet.png)

our data while traveling
=================

This chapter briefly describes the path our data takes as
in a `browser` browser such as` firefox`firefox we enter a web page of the
View VRT (from teletext). You can view this chapter as one
brief overview of the whole course, everything we look at in the course
can be placed somewhere in this story.

layer 7: application
------------------

The journey starts in the top layer. That is layer 7, the application layer. We
use as an example the application called `firefox`mozilla firefox
on which page 525 of vrt teletext is open. Of course the
application `firefox` is located in the top layer.

We clicked the 'show' button to refresh the page. Click on
that button sends an `http request`http to the vrt teletext
`webserver`webserver. The `layer 7 protocol`layer 7 protocol true
web servers and web clients (aka browsers) work with it
`http (hyper text transfer protocol)` http. Our data drops to layer 6.

layers 6 and 5: presentation and session
-----------------------------------

These layers don't really apply because we use `tcp / ip`tcp / ip
and not with a 7-layer osi protocol. You could say that the
`browser` in layer 6 receives a` html` document from the `webserver`. This
`html` document can consist of` ascii` or `unicode` characters.

layer 4: transport
-----------------

In this layer we see how the `tcp`tcp of` tcp / ip` sets up a `session`.

As previously written, there is some cut and paste work in this layer
place. Only it is just a case where there is no cutting.
We request a `web page` from a` web server`, and this question fits
perfect in a pack, so there is no cutting. The protocol
at work here is `tcp (Transfer Control Protocol)` transfer control
protocol.

But there is still something more than just sending the question
`firefox` to the vrt teletext` webserver`. The `tcp` protocol goes
first set up a `tcp session`tcp session with the vrt webserver. We
can check this by starting a `sniffer`sniffer.

You will see three `tcp packets` before the` http` packet (which is also one
tcp packet is sent. `tcp` sets up a session by first one
send a packet asking to set up a session (SYN), on this
an answer comes from the VRT web server (SYN, ACK) and finally there is
sent another (ACK) from my laptop.

In the screenshot above you can see a `http GET`http of page 511
instead of page 525, but that has no importance for the theory.

layer 3: network
---------------

Our question for a web page should go to the `webserver` of
the vrt. This computer is not in our class (nor with me
home). The `ip` protocol ensures the delivery of the parcel
location in the Reyerslaan (where this vrt webserver is located).

To get to the correct place, an `ip address` ip address is required.
The IP address of the vrt web server is `193.191.175.137`. We find this
back in every packet we send to the web server of the vrt, in the
provide space for `destination IP address`. If source becomes IP address
every time use the ip address of my laptop at home, in this case
`192.168.1.34`.

Our sniffer translates the `hexadecimal code`hexadecimal representing the computer
used to human readable `decimal numbers`.

How can our computer change the `ip address` of the web server of vrt teletext
know? For this, `tcp / ip`tcp / ip uses` dns`dns. Incase you
goes to teletext website for the first time in a while
`url`` http: // teletext.een.be`, there will be a `dns query` from our
computer to our local `DNS server`dns server.

DNS (Domain Name System) `is discussed in detail later, but it
you might see the following in the sniffer.

layer 2: data link
-----------------

The `OSI data link layer` is part of the` DoD link layer` of
`tcp / ip`. In this layer we use the `MAC`mac address of a
network card. A `mac address` is a physical address that is burned into
the network card.

Before our first packet can leave, the `mac address' of the
local destination. The web server of vrt teletext is up
not at my house so the local destination is my `router` router (aka
the adsl modem). It has `ip address` 192.168.1.1.

My laptop will output an `arp`arp broadcast to the mac address
find the computer with ip address 192.168.1.1. My `router` will
answer this. In a sniffer this looks like this.

layer 1: physical
----------------

The network card is happy when the packet is complete and throws it
literally on my local network. Physically this is a broadcast, all
computers in the local network receive this packet, but only the
any correct recipient will also accept the package. That
correct destination is my local `router`.

In the OSI world this is a separate layer, in the DoD world this is one
part of the `DoD link layer`link layer.

our data on the go
------------------

Our `http request`http for a teletext page is now underway from
Antwerp center to the Reyerslaan in Brussels. The packet jumps
from router to router, until it is on the network of the `webserver` of
the vrt is.

You can view this road (the routers) by using a `traceroute` traceroute
to do. Below you can see the output of the `traceroute` command on my
Laptop.

    traceroute to 193.191.175.137 (193.191.175.137), 30 hops max, 60 byte packets
    1 illyria (192.168.1.1) 0.812 ms 1,285 ms 1,764 ms
    2 213.219.148.1.adsl.dyn.edpnet.net (213.219.148.1) 26.453 ms 26.833 ms 27.216 ms
    3 ndl2-rb01.edpnet.be (213.219.132.237) 27.655 ms 28.035 ms 28.412 ms
    4 adslgwbe.edpnet.net (212.71.1.78) 28.924 ms 29.305 ms 29.798 ms
    5 10ge.cr1.brueve.belnet.net (194.53.172.65) 30,165 ms 30,543 ms 30,888 ms
    6 10ge.ar1.brucam.belnet.net (193.191.16.193) 31.991 ms 31.530 ms 31.960 ms
    7 vrt.customer.brussels.belnet.net (193.191.4.189) 32.046 ms 9.004 ms 24.205 ms
    8 * * *
    9 * * *
        

What you see is that the first router is called `illyria` and` 192.168.1.1` as
IP address. This is my local `adsl modem`adsl with built-in
router function. On the second line you see the other side of my adsl
modem, called my internet service provider (ISP) side
`edpnet`edpnet.

Then we pass some edpnet routers and then travel
to `belnet`. The Belgian `belnet`belnet is a fast backbone network
which include universities and public institutions.

The last router we see is called `vrt.customer.brussels.belnet.net`.
From this name you can deduce that the VRT is a customer of `belnet` in
Brussels.

Afterwards we only see asterisks, that is because the `system administrator`
of the VRT has decided not to follow a 'traceroute'
leave `firewall` firewall. Fortunately, our `http request` is allowed.

destination reached
------------------

If all goes well, then our data (or our `http request`) will be reached
destination. The last 'router' throws our data on the local network
from the `webserver` of the VRT.

The network card of this server converts the electrical signals
bytes, and checks if the first six bytes match her
`MAC address` mac. Fortunately, that is correct, so it is the turn of `IP`.

The `ip protocol` checks whether the` destination ip address` is
is correct, and (luckily) that is correct again.

Further `metadata` in the packet tells this computer that it is one
is `tcp packet` with destination` http server`http.

The `http server (or web server)` then searches for the requested web page,
and sends this webpage in response to our question.

there will be an answer
----------------

The web server has the conscious `web page` ready and wants an answer
send to my laptop. This computer knows my `ip address` ip address
because this is included in the package as `source ip-address`.

We are back from the `application layer` (yes an http server is also an
application) down.

By the way, the `presentation` of this data to the browser happens
with a `mime type`. Email messages (and websites) have not existed for a long time
more just from `ascii`ascii characters, but also contain others
characters, photos, sound and film.

`MIME (Multipurpose Internet Mail Extensions)` mime defines a
mechanism to send this content over email (or as in us
example on `http`). You could put `mime` in` osi layer 6`.

The `tcp-session`tcp session that my laptop set up is still there
always. The answer can immediately go through the `transport layer` to
`ip`, or almost. After all, the requested web page is too large to be 1
packet to be forwarded, so cutting and pasting is required
`tcp`. You can see this in the sniffer as a sequence of
`tcp segment` packages, followed by a` tcp ack`tcpnowledgement.

The `ip`ip protocol will ensure the return from Brussels to
Antwerp, possibly via other `routers`.

Once the answer is back on our network, the data goes from bottom to bottom
up through the layers, until our `browser` can show the web page.

ask
------

1 \. How does the computer of the VRT know that the packet to the http server
must ?

2 \. How does my laptop know which application the package is for?

layers of exercise
==============

0 \. Choose a network for the following questions: home, work, class

1 \. Make a drawing (paper or PC) of your network for layer 1.

2 \. Make a drawing (paper or PC) of your network for layer 2.

3 \. Make a drawing (paper or PC) of your network for layer 3 (when
you visit a website).

4 \. Make a drawing (paper or PC) of your network for layer 7 (when
you visit a website).
