## About this Site

This website gives free resources to study Linux. A lot of the content is written by Paul Cobbaut. He hasn't updated it since may 2015.

>"I stopped putting free updates online because many people copy my work without giving me credit, putting their own name and advertising on it."

### [Linux Fundamentals](Introduction/01.Linux-history.html)

Teaches you the very fundamentals of Linux. Made for beginners, novices, anyone who wants to learn Linux. It contains basic knowledge. Advanced users can also benefit from this book.

### [Linux Administration](process_management/01.processes_intro.html)

Teaches you Linux system administration (not Unix). Anything that is related to Linux system management, is introduced in this book.

### [Linux Storage](local_disk_management/01.storage_devices.html)

Explains disk management on Linux in detail and introduces a lot of other storage related technologies.

### [Linux Security](/users/01.users.html)

Focuses on all topics related to security such as file permissions, acls, SELinux, users and passwords, and more.

### [Networking](networking/01.general_networking.html)
Focuses on all things related to Linux in a network.

## LICENSE

The books are Licensed under the [GNU Free Documentation License](https://www.gnu.org/licenses/fdl.html)

---

More information available at http://linux-training.be
