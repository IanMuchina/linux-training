## Frequently Asked Questions

### How can I learn Linux ?
Please guide me in mastering Linux ?
Please advice on how to get a job in Linux?
What can I do to gain experience?
This question gets asked a lot!

#### Answer:

*Step 1*: Use Linux at home.
Set up dhcp and dns at home with Linux (and remove these services from your router/modem). Experiment with domain blocking in dns, with naming all your devices, with fixing ip-addresses using dns and dhcp.
Use Linux to automate backups for yourself and your family.
Create your own Linux web server and let your friends look at the website.

*Step 2*: Give **solutions** to others
Find a local organisation that you can help, a hobby club, or an animal asylum or anything. Listen to their problems (or their wishes) and provide a solution. Help them, even for free, this helps you build a portfolio.

In general **do not** advertise Linux, do not try to "convert" people to Linux.
Instead make sure you provide a solution that makes them happy.

*Step 3*: Go back to step 0 and read all the free books on this website.

### Why do you give everything away for free ?

Many people give their work away for free. I like this system where we all work together and share. The opposite would force every Linux teacher to do the same work that I already did, what would be the point of that?

One nice advantage is that I often get advice from specialists about certain topics.
If something is unclear in my books, let me know, it can benefit the next reader!

### How do you make money ?
**Answer**: I mainly work as a consultant for NetSec BVBA. About half of my work is teaching (for really big companies like Google, banks, telecom, insurance...). The other half is consultancy, usually for smaller organisations (50 to 2000 employees).

### Question: Why are there no changes after 25 May 2015 ?!

**Answer**: I stopped putting free updates online because many people copy my work without giving me credit, putting their own name and advertising on it.