This chapter will teach you how to use `useradd`, `usermod` and
`userdel` to create, modify and remove user accounts.

You will need `root` access on a Linux computer to complete this
chapter.
user management
===============

User management on Linux can be done in three complementary ways. You
can use the `graphical` tools provided by your distribution. These tools
have a look and feel that depends on the distribution. If you are a
novice Linux user on your home system, then use the graphical tool that
is provided by your distribution. This will make sure that you do not
run into problems.

Another option is to use `command line tools` like useradd, usermod,
gpasswd, passwd and others. Server administrators are likely to use
these tools, since they are familiar and very similar across many
different distributions. This chapter will focus on these command line
tools.

A third and rather extremist way is to
`edit the local configuration files` directly using vi (or vipw/vigr).
Do not attempt this as a novice on production systems!

/etc/passwd
===========

The local user database on Linux (and on most Unixes) is
`/etc/passwd`/etc/passwd.

    [root@RHEL5 ~]# tail /etc/passwd
    inge:x:518:524:art dealer:/home/inge:/bin/ksh
    ann:x:519:525:flute player:/home/ann:/bin/bash
    frederik:x:520:526:rubius poet:/home/frederik:/bin/bash
    steven:x:521:527:roman emperor:/home/steven:/bin/bash
    pascale:x:522:528:artist:/home/pascale:/bin/ksh
    geert:x:524:530:kernel developer:/home/geert:/bin/bash
    wim:x:525:531:master damuti:/home/wim:/bin/bash
    sandra:x:526:532:radish stresser:/home/sandra:/bin/bash
    annelies:x:527:533:sword fighter:/home/annelies:/bin/bash
    laura:x:528:534:art dealer:/home/laura:/bin/ksh

As you can see, this file contains seven columns separated by a colon.
The columns contain the username, an x, the user id, the primary group
id, a description, the name of the home directory, and the login shell.

More information can be found by typing `man 5 passwd`.

    [root@RHEL5 ~]# man 5 passwd

root
====

The `root`root user also called the `superuser`superuser is the most
powerful account on your Linux system. This user can do almost anything,
including the creation of other users. The root user always has userid 0
(regardless of the name of the account).

    [root@RHEL5 ~]# head -1 /etc/passwd
    root:x:0:0:root:/root:/bin/bash

useradd
=======

You can add users with the `useradd`useradd command. The example below
shows how to add a user named yanina (last parameter) and at the same
time forcing the creation of the home directory (-m), setting the name
of the home directory (-d), and setting a description (-c).

    [root@RHEL5 ~]# useradd -m -d /home/yanina -c "yanina wickmayer" yanina
    [root@RHEL5 ~]# tail -1 /etc/passwd
    yanina:x:529:529:yanina wickmayer:/home/yanina:/bin/bash

The user named yanina received userid 529 and `primary group`primary
group id 529.

/etc/default/useradd
====================

Both Red Hat Enterprise Linux and Debian/Ubuntu have a file called
`/etc/default/useradd`/etc/default/useradd that contains some default
user options. Besides using cat to display this file, you can also use
`useradd -D`useradd -D.

    [root@RHEL4 ~]# useradd -D
    GROUP=100
    HOME=/home
    INACTIVE=-1
    EXPIRE=
    SHELL=/bin/bash
    SKEL=/etc/skel

userdel
=======

You can delete the user yanina with `userdel`userdel(1). The -r option
of userdel will also remove the home directory.

    [root@RHEL5 ~]# userdel -r yanina

usermod
=======

You can modify the properties of a user with the `usermod`usermod(1)
command. This example uses `usermod` to change the description of the
user harry.

    [root@RHEL4 ~]# tail -1 /etc/passwd
    harry:x:516:520:harry potter:/home/harry:/bin/bash
    [root@RHEL4 ~]# usermod -c 'wizard' harry
    [root@RHEL4 ~]# tail -1 /etc/passwd
    harry:x:516:520:wizard:/home/harry:/bin/bash

creating home directories
=========================

The easiest way to create a home directory is to supply the `-m` option
with `useradd`useradd(1) (it is likely set as a default option on
Linux).

A less easy way is to create a home directory manually with `mkdir`mkdir
which also requires setting the owner and the permissions on the
directory with `chmod`chmod and `chown`chown (both commands are
discussed in detail in another chapter).

    [root@RHEL5 ~]# mkdir /home/laura
    [root@RHEL5 ~]# chown laura:laura /home/laura
    [root@RHEL5 ~]# chmod 700 /home/laura
    [root@RHEL5 ~]# ls -ld /home/laura/
    drwx------ 2 laura laura 4096 Jun 24 15:17 /home/laura/

/etc/skel/
==========

When using `useradd` the `-m` option, the `/etc/skel/`/etc/skel
directory is copied to the newly created home directory. The
`/etc/skel/` directory contains some (usually hidden) files that contain
profile settings and default values for applications. In this way
`/etc/skel/` serves as a default home directory and as a default user
profile.

    [root@RHEL5 ~]# ls -la /etc/skel/
    total 48
    drwxr-xr-x  2 root root  4096 Apr  1 00:11 .
    drwxr-xr-x 97 root root 12288 Jun 24 15:36 ..
    -rw-r--r--  1 root root    24 Jul 12  2006 .bash_logout
    -rw-r--r--  1 root root   176 Jul 12  2006 .bash_profile
    -rw-r--r--  1 root root   124 Jul 12  2006 .bashrc

deleting home directories
=========================

The -r option of `userdel`useradd will make sure that the home directory
is deleted together with the user account.

    [root@RHEL5 ~]# ls -ld /home/wim/
    drwx------ 2 wim wim 4096 Jun 24 15:19 /home/wim/
    [root@RHEL5 ~]# userdel -r wim
    [root@RHEL5 ~]# ls -ld /home/wim/
    ls: /home/wim/: No such file or directory

login shell
===========

The `/etc/passwd`/etc/passwd file specifies the `login shell` for the
user. In the screenshot below you can see that user annelies will log in
with the `/bin/bash` shell, and user laura with the `/bin/ksh` shell.

    [root@RHEL5 ~]# tail -2 /etc/passwd
    annelies:x:527:533:sword fighter:/home/annelies:/bin/bash
    laura:x:528:534:art dealer:/home/laura:/bin/ksh

You can use the usermod command to change the shell for a user.

    [root@RHEL5 ~]# usermod -s /bin/bash laura
    [root@RHEL5 ~]# tail -1 /etc/passwd
    laura:x:528:534:art dealer:/home/laura:/bin/bash

chsh
====

Users can change their login shell with the `chsh`chsh(1) command.
First, user harry obtains a list of available shells (he could also have
done a `cat /etc/shells`/etc/shells) and then changes his login shell to
the `Korn shell`Korn Shell (/bin/ksh). At the next login, harry will
default into ksh instead of bash.

    [laura@centos7 ~]$ chsh -l
    /bin/sh
    /bin/bash
    /sbin/nologin
    /usr/bin/sh
    /usr/bin/bash
    /usr/sbin/nologin
    /bin/ksh
    /bin/tcsh
    /bin/csh
    [laura@centos7 ~]$

Note that the `-l` option does not exist on Debian and that the above
screenshot assumes that `ksh` and `csh` shells are installed.

The screenshot below shows how `laura` can change her default shell
(active on next login).

    [laura@centos7 ~]$ chsh -s /bin/ksh
    Changing shell for laura.
    Password: 
    Shell changed.
practice: user management
=========================

1\. Create a user account named `serena`, including a home directory and
a description (or comment) that reads `Serena Williams`. Do all this in
one single command.

2\. Create a user named `venus`, including home directory, bash shell, a
description that reads `Venus Williams` all in one single command.

3\. Verify that both users have correct entries in `/etc/passwd`,
`/etc/shadow` and `/etc/group`.

4\. Verify that their home directory was created.

5\. Create a user named `einstime` with `/bin/date` as his default logon
shell.

6\. What happens when you log on with the `einstime` user ? Can you think
of a useful real world example for changing a user\'s login shell to an
application ?

7\. Create a file named `welcome.txt` and make sure every new user will
see this file in their home directory.

8\. Verify this setup by creating (and deleting) a test user account.

9\. Change the default login shell for the `serena` user to `/bin/bash`.
Verify before and after you make this change.
solution: user management
=========================

1\. Create a user account named `serena`, including a home directory and
a description (or comment) that reads `Serena Williams`. Do all this in
one single command.

    root@debian7:~# useradd -m -c 'Serena Williams' serena

2\. Create a user named `venus`, including home directory, bash shell, a
description that reads `Venus Williams` all in one single command.

    root@debian7:~# useradd -m -c "Venus Williams" -s /bin/bash venus

3\. Verify that both users have correct entries in `/etc/passwd`,
`/etc/shadow` and `/etc/group`.

    root@debian7:~# tail -2 /etc/passwd
    serena:x:1008:1010:Serena Williams:/home/serena:/bin/sh
    venus:x:1009:1011:Venus Williams:/home/venus:/bin/bash
    root@debian7:~# tail -2 /etc/shadow
    serena:!:16358:0:99999:7:::
    venus:!:16358:0:99999:7:::
    root@debian7:~# tail -2 /etc/group
    serena:x:1010:
    venus:x:1011:

4\. Verify that their home directory was created.

    root@debian7:~# ls -lrt /home | tail -2
    drwxr-xr-x 2 serena    serena    4096 Oct 15 10:50 serena
    drwxr-xr-x 2 venus     venus     4096 Oct 15 10:59 venus
    root@debian7:~#

5\. Create a user named `einstime` with `/bin/date` as his default logon
shell.

    root@debian7:~# useradd -s /bin/date einstime

Or even better:

    root@debian7:~# useradd -s $(which date) einstime

6\. What happens when you log on with the `einstime` user ? Can you think
of a useful real world example for changing a user\'s login shell to an
application ?

    root@debian7:~# su - einstime
    Wed Oct 15 11:05:56 UTC 2014    # You get the output of the date command
    root@debian7:~#

It can be useful when users need to access only one application on the
server. Just logging in opens the application for them, and closing the
application automatically logs them out.

7\. Create a file named `welcome.txt` and make sure every new user will
see this file in their home directory.

    root@debian7:~# echo Hello > /etc/skel/welcome.txt

8\. Verify this setup by creating (and deleting) a test user account.

    root@debian7:~# useradd -m test
    root@debian7:~# ls -l /home/test
    total 4
    -rw-r--r-- 1 test test 6 Oct 15 11:16 welcome.txt
    root@debian7:~# userdel -r test
    root@debian7:~#

9\. Change the default login shell for the `serena` user to `/bin/bash`.
Verify before and after you make this change.

    root@debian7:~# grep serena /etc/passwd
    serena:x:1008:1010:Serena Williams:/home/serena:/bin/sh
    root@debian7:~# usermod -s /bin/bash serena
    root@debian7:~# grep serena /etc/passwd
    serena:x:1008:1010:Serena Williams:/home/serena:/bin/bash
    root@debian7:~#
